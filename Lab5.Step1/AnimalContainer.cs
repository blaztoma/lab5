﻿namespace Lab5.Step1
{
    class AnimalContainer
    {
        private Animal[] animals;
        public int Count {get; set;}

        public AnimalContainer()
        {
            animals = new Animal[Program.MaxNumberOfAnimals];
            Count = 0;
        }

        public AnimalContainer(int size)
        {
            animals = new Animal[size];
            Count = 0;
        }

        public void Add(Animal animal)
        {
            animals[Count] = animal;
            Count++;
        }

        public void Set(int index, Animal animal)
        {
            animals[index] = animal;
        }

        public Animal Get(int index)
        {
            return animals[index];
        }

        public bool Contains(Animal animal)
        {
            for (int i = 0; i < Count; i++)
            {
                if (animals[i].Equals(animal))
                {
                    return true;
                }
            }
            return false;
        }

        public Animal GetAnimalByChipId(int chipId)
        {
            for (int i = 0; i < Count; i++)
            {
                if (animals[i].ChipId == chipId)
                {
                    return animals[i];
                }
            }
            return null;
        }
    }
}
