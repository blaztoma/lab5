﻿namespace Lab5.Step1
{
    class Branch
    {

        public string Town { get; set; }
        public AnimalContainer Dogs { get; set; }
        public AnimalContainer Cats { get; set; }
        public CalendarContainer Calendar { get; set; }


        public Branch(string town)
        {
            Town = town;
            Dogs = new AnimalContainer();
            Cats = new AnimalContainer();
            Calendar = new CalendarContainer();
        }
    }
}
