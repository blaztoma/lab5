﻿using System;
using System.IO;
using System.Linq;

namespace Lab5.Step1
{
    class Program
    {
        public const int NumberOfBranches = 3;
        public const int MaxNumberOfBreeds = 10;
        public const int MaxNumberOfAnimals = 50;

        public static int NumberOfDays = 7; 
        public static int NumberOfWorkHours = 9; 
        public static int workStartsAt = 8;
        public static int workFinishesAt = 17;

        public enum WeekDays { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday };

        static void Main(string[] args)
        {
            Branch[] branches = new Branch[NumberOfBranches];
            int[] workingHours = new int[NumberOfWorkHours];

            branches[0] = new Branch("Kaunas");
            branches[1] = new Branch("Vilnius");
            branches[2] = new Branch("Šiauliai");

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.csv");

            foreach (string path in filePaths)
            {
                ReadAnimalData(path, branches);
            }

            Console.WriteLine("Kaune užregistruoti šunys:");
            PrintAnimalsToConsole(branches[0].Dogs);
            Console.WriteLine();

            string[] calendarFilePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.cal");

            foreach (string path in calendarFilePaths)
            {
                ReadCalendarData(path, branches);
            }

            branches[0].Calendar.PrintCalendar(branches[0].Dogs, branches[0].Cats);

            // ar galima registruotis Kaune antradienį, 11h?
            int day = (int)WeekDays.Tuesday;
            int hour = 11; 

            bool isFree = branches[0].Calendar.isTimeFreeToRegister(day, hour - workStartsAt);

            if (isFree)
            {
                Console.WriteLine("Registruotis galima!");
            }
            else
            {
                Console.WriteLine("Deja, registruotis negalima, pasirinkite kitą laiką");
            }

            //rasti darbo valandas antradienį
            Console.WriteLine("Laisvos vakcinacijos valandos antradienį:");
            int workingHoursCount = 0;
            workingHours = branches[0].Calendar.GetWorkingHours((int)WeekDays.Tuesday, out workingHoursCount);
            for (int i = 0; i < workingHoursCount; i++)
            {
                Console.Write(workingHours[i] + workStartsAt + " ");
            }

            // didžiausias užimtumas
            Console.WriteLine();
            Console.Write("Diena, kurią didžiausias užimtumas: ");
            int mostOccupiedDay = branches[0].Calendar.GetMostOccupiedDay();
            Console.WriteLine(Enum.GetName(typeof(WeekDays), mostOccupiedDay));

            Console.Read();
        }

        static void PrintAnimalsToConsole(AnimalContainer animals)
        {
            for (int i = 0; i < animals.Count; i++)
            {
                Console.WriteLine("Nr {0,-2}: {1}", (i + 1), animals.Get(i).ToString());
            }
        }

        private static Branch GetBranchByTown(Branch[] branches, string town)
        {
            for (int i = 0; i < NumberOfBranches; i++)
            {
                if (branches[i].Town == town)
                {
                    return branches[i];
                }
            }
            return null;
        }

        private static void ReadAnimalData(string file, Branch[] branches)
        {

            string town = null;

            using (StreamReader reader = new StreamReader(@file))
            {
                string line = null;
                line = reader.ReadLine();
                if (line != null)
                {
                    town = line;
                }
                Branch branch = GetBranchByTown(branches, town);
                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(',');
                    char type = Convert.ToChar(line[0]);
                    string name = values[1];
                    int chipId = int.Parse(values[2]);
                    string breed = values[3];
                    string owner = values[4];
                    string phone = values[5];
                    DateTime vd = DateTime.Parse(values[6]);

                    switch (type)
                    {
                        case 'D':
                            //atkreipkite dėmesį - šunys turi papildomą požymį "aggressive"
                            bool aggressive = bool.Parse(values[7]);
                            Dog dog = new Dog(name, chipId, breed, owner, phone, vd, aggressive);
                            if (!branch.Dogs.Contains(dog))
                            {
                                branch.Dogs.Add(dog);
                            }
                            break;
                        case 'C':
                            Cat cat = new Cat(name, chipId, breed, owner, phone, vd);
                            if (!branch.Cats.Contains(cat))
                            {
                                branch.Cats.Add(cat);
                            }
                            break;
                    }
                }
            }
        }

        private static void ReadCalendarData(string file, Branch[] branches)
        {
            string town = null;

            using (StreamReader reader = new StreamReader(@file))
            {
                string line = null;
                line = reader.ReadLine();
                if (line != null)
                {
                    town = line;
                }
                Branch branch = GetBranchByTown(branches, town);

                int day = 0;
                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(',');
                    for (int time = 0; time < values.Length; time++)
                    {
                        int value = int.Parse(values[time]);
                        branch.Calendar.SetTimeTableValue(day, time, value);
                    }
                    day++;  
                }
            }
        }

        private static void GetBreeds(AnimalContainer animals, out string[] breeds, out int breedCount)
        {
            breeds = new string[MaxNumberOfBreeds];
            breedCount = 0;

            for (int i = 0; i < animals.Count; i++)
            {
                if (!breeds.Contains(animals.Get(i).Breed))
                {
                    breeds[breedCount++] = animals.Get(i).Breed;
                }
            }
        }


        private static void FilterByBreed(AnimalContainer animals, string breed, AnimalContainer filteredAnimals)
        {
            for (int i = 0; i < animals.Count; i++)
            {
                if (animals.Get(i).Breed == breed)
                {
                    filteredAnimals.Add(animals.Get(i));
                }
            }
        }

        private static int CountAggressive(AnimalContainer dogs)
        {
            int counter = 0;
            for (int i = 0; i < dogs.Count; i++)
            {
                if ((dogs.Get(i) as Dog).Aggressive)
                {
                    counter++;
                }
            }

            return counter;
        }

        private static string GetMostPopularBreed(AnimalContainer animals)
        {
            String popular = "not found";
            int count = 0;

            int breedCount = 0;
            string[] breeds;

            GetBreeds(animals, out breeds, out breedCount);

            for (int i = 0; i < breedCount; i++)
            {
                AnimalContainer filteredAnimals = new AnimalContainer();
                FilterByBreed(animals, breeds[i], filteredAnimals);
                if (filteredAnimals.Count > count)
                {
                    popular = breeds[i];
                    count = filteredAnimals.Count;
                }
            }

            return popular;
        }

        //funkcija gražins surikiuotą gyvūnų sąrašą
        private static AnimalContainer SortAnimals(AnimalContainer animals)
        {
            for (int i = 0; i < animals.Count - 1; i++)
            {
                Animal minValueAnimal = animals.Get(i);
                int minValueIndex = i;
                for (int j = i + 1; j < animals.Count; j++)
                {
                    if (animals.Get(j) <= minValueAnimal)
                    {
                        minValueAnimal = animals.Get(j);
                        minValueIndex = j;
                    }
                }
                animals.Set(minValueIndex, animals.Get(i));
                animals.Set(i, minValueAnimal);
            }
            return animals;
        }
    }
}
