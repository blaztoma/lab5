﻿using System;
namespace Lab5.Step1
{
    class CalendarContainer
    {
        private int[,] timeTable = new int[Program.NumberOfDays, Program.NumberOfWorkHours];

        public void SetTimeTableValue(int day, int hour, int id)
        {
            timeTable[day, hour] = id;
        }

        public int GetTimeTableValue(int day, int hour)
        {
            return timeTable[day,hour];
        }

        public bool isTimeFreeToRegister( int day, int hour)
        {
            return GetTimeTableValue(day, hour) == 0;
        }

        public int[] GetWorkingHours(int day, out int workingHoursCount)
        {
            int[] freeHours = new int[Program.NumberOfWorkHours];
            workingHoursCount = 0;
            for (int i = 0; i < Program.NumberOfWorkHours; i++)
            {
                if (timeTable[day, i] == 0)
                {
                    freeHours[workingHoursCount++] = i;
                }
            }
            return freeHours;
        }

        public int GetMostOccupiedDay()
        {
            int mostOccupiedDay = (int)Program.WeekDays.Monday;
            int mostOccupiedCount = 0;
            for (int i = (int)Program.WeekDays.Monday; i < (int)Program.WeekDays.Sunday; i++)
            {
                int registeredAnimalCount = 0;
                for (int j = Program.workStartsAt; j < Program.workFinishesAt; j++)
                {
                    if (timeTable[i, j - Program.workStartsAt] != 0)
                    {
                        registeredAnimalCount++;
                    }
                }
                if (registeredAnimalCount > mostOccupiedCount)
                {
                    mostOccupiedDay = i;
                    mostOccupiedCount = registeredAnimalCount;
                }
            }
            return mostOccupiedDay;
        }

        public void PrintCalendar(AnimalContainer dogs, AnimalContainer cats)
        {
            for (int i = (int)Program.WeekDays.Monday; i < (int)Program.WeekDays.Sunday; i++)
            {
                Console.WriteLine(Enum.GetName(typeof(Program.WeekDays), i) + ":");
                for (int j = Program.workStartsAt; j < Program.workFinishesAt; j++)
                {
                    switch (timeTable[i, j - Program.workStartsAt])
                    {
                        case -1:
                            Console.WriteLine(j + " val. nedirba");
                            break;
                        case 0:
                            Console.WriteLine(j + " val. laisva");
                            break;
                        default:
                            Animal dog = dogs.GetAnimalByChipId(timeTable[i, j - Program.workStartsAt]);
                            if (dog != null)
                            {
                                Console.WriteLine(j + " val. resgistruotas šuo " + dog.Name + ", kurio šeimininkas: " + dog.Owner);
                                break;
                            }
                            Animal cat = cats.GetAnimalByChipId(timeTable[i, j - Program.workStartsAt]);
                            if (dog != null)
                            {
                                Console.WriteLine(j + " val. resgistruota katė " + cat.Name + ", kurio sšeimininkas: " + cat.Owner);
                                break;
                            }
                            Console.WriteLine(j + " val.: registracijos klaida - tokia mikroschema neegzistuoja.");
                            break;
                    }
                }
            }
        }
    }
}
