﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Lab4.Converter
{
    class Program
    {
        const string htmlDataFile = @"Klaipeda.html";
        const string csvDataFile = @"Klaipeda.csv";
        const string wrongDataFile = @"KlaidingiDuomenys.csv";
        const string htmlOutputFile = @"Klaipeda2.html";

        const string tablePattern = "<table.*?>(.*?)</table>";
        const string thPattern = "<th.*?>(.*?)</th>";
        const string trPattern = "<tr>(.*?)</tr>";
        const string tdPattern = "<td.*?>(.*?)</td>";

        const string csvSeparator = ";";

        enum Fields { AnimalType, AnimalName, AnimalChip, AnimalBreed, OwnerName, OwnerPhone, VaccinationDate, AgressiveState };

        static void Main(string[] args)
        {
            string[] csvLines = ConvertToCsv(htmlDataFile);

            csvLines = RemoveTags(csvLines);

            csvLines = FixNames(csvLines, (int)Fields.AnimalName);
            csvLines = FixNames(csvLines, (int)Fields.OwnerName);

            csvLines = FixPhoneNumbers(csvLines, (int)Fields.OwnerPhone);

            int wrongCount;
            string[] wrongDataLines = GetLinesWithWrongNumbers(csvLines, (int)Fields.OwnerPhone, out wrongCount);
            WriteLinesToFile(wrongDataFile, wrongDataLines, wrongCount);

            int correctCount;
            string[] correctDataLines = RemoveWrongDataLines(csvLines, wrongDataLines, wrongCount, out correctCount);
            WriteLinesToFile(csvDataFile, correctDataLines, correctCount);

            string htmlContent = ConvertToHtml(correctDataLines, correctCount);
            using (var fileHandle = File.CreateText(htmlOutputFile))
            {
                fileHandle.WriteLine(htmlContent);
            }

            foreach (string line in csvLines)
            {
                Console.WriteLine(line);
            }

            Console.Read();
        }

        private static string[] RemoveWrongDataLines(string[] csvLines, string[] wrongDataLines, int wrongCount, out int correctCount)
        {
            string[] correctDataLines = new string[csvLines.Length];

            correctCount = 0;
            for (int i = 1; i < csvLines.Length; i++)
            {
                bool foundEqual = false;
                for (int j = 0; j < wrongCount; j++)
                {
                    if(csvLines[i].Equals(wrongDataLines[j]))
                    {
                        foundEqual = true;
                    }
                }
                if(!foundEqual) correctDataLines[correctCount++] = csvLines[i];
            }
            return correctDataLines;
        }

        private static void WriteLinesToFile(string dataFile, string[] data, int dataCount)
        {
            using (var fileHandle = File.CreateText(dataFile))
            {
                for (int i = 0; i < dataCount; i++)
                {
                    fileHandle.WriteLine(data[i]);
                }
            }
        }

        private static string[] FixNames(string[] csvLines, int fieldToCorrect)
        {
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] stringFields = csvLines[i].Split(csvSeparator[0]);
                if(Char.IsLower(stringFields[fieldToCorrect][0]))
                {
                    stringFields[fieldToCorrect] = stringFields[fieldToCorrect].Substring(0, 1).ToUpper() + stringFields[fieldToCorrect].Substring(1).ToLower();
                    csvLines[i] = string.Join(csvSeparator, stringFields);
                }
            }
            return csvLines;
        }
/*
        public static bool IsPhoneNumber(string number)
        {
            if(number[0] != '+') return false;
            if (number.Length != 12) return false;
            
            for(int i = 1; i < number.Length; i++)
            {
                if((number[i] < '0')||(number[i] > '9')) return false;
            } 
            return true;
        }
*/

        public static bool IsPhoneNumber(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{11})$").Success;
        }

        private static string[] FixPhoneNumbers(string[] csvLines, int fieldToCorrect)
        {
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] stringFields = csvLines[i].Split(csvSeparator[0]);
                if (!IsPhoneNumber(stringFields[fieldToCorrect]))
                {
                    if (stringFields[fieldToCorrect][0] == '8')
                    {
                        stringFields[fieldToCorrect] = "+370" + stringFields[fieldToCorrect].Substring(1);
                        csvLines[i] = string.Join(csvSeparator, stringFields);
                    }
                }
            }
            return csvLines;
        }

        private static string[] GetLinesWithWrongNumbers(string[] csvLines, int fieldToCheck, out int wrongCount)
        {
            string[] wrongDataLines = new string[csvLines.Length];

            wrongCount = 0;
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] stringFields = csvLines[i].Split(csvSeparator[0]);
                if (!IsPhoneNumber(stringFields[fieldToCheck]))
                {
                    wrongDataLines[wrongCount++] = csvLines[i];
                }
            }
            return wrongDataLines;
        }

        private static string[] RemoveTags(string[] csvLines)
        {
            for(int i = 0; i<csvLines.Length;i++)
            {
                csvLines[i] = Regex.Replace(csvLines[i], "<.*?>", string.Empty);
            }
            return csvLines;
        }

        private static string[] ConvertToCsv(string fileName)
        {
            string fileContent = File.ReadAllText(fileName);

            MatchCollection table_matches = Regex.Matches(fileContent, tablePattern, RegexOptions.Singleline);
            string tableString = table_matches[0].Value; //ištraukiam visą lentelę, kitkas mums neįdomu.

            MatchCollection tr_matches = Regex.Matches(tableString, trPattern, RegexOptions.Singleline); //ieškome visų lentelės eilučių

            string[] csvLines = new string[tr_matches.Count];

            MatchCollection th_matches = Regex.Matches(tr_matches[0].Value, thPattern, RegexOptions.Singleline); //pirmoje eilutėje ieškome antraščių
            string title = th_matches[0].Groups[1].Value; //imame pirmąją antraštę
            csvLines[0] = title;

            for (int i = 1; i < tr_matches.Count; i++)//einame per likusias eilutes
            {
                StringBuilder line = new StringBuilder();
                MatchCollection td_matches = Regex.Matches(tr_matches[i].Value, tdPattern, RegexOptions.Singleline);

                for (int j = 0; j < td_matches.Count; j++)//einame per stulpelius
                {
                    line.Append(td_matches[j].Groups[1].Value).Append(csvSeparator);//nuskaitom lentelės langelio reikšmę ir dedam į eilutę, po jos dedam skyriklį
                }

                csvLines[i] = line.ToString();
            }

            return csvLines;
        }

        private static string ConvertToHtml(string[] data, int dataCount)
        {
            string headerContent = File.ReadAllText(@"HeaderTemplate.html");
            string footerContent = File.ReadAllText(@"FooterTemplate.html");
            string dataLineContent = File.ReadAllText(@"DataLineTemplate.html");

            StringBuilder htmlContent = new StringBuilder();

            htmlContent.Append(headerContent);

            for (int i = 0; i < dataCount; i++)
            {
                string[] lineFields = data[i].Split(csvSeparator[0]);
                string processedLine = dataLineContent;
                processedLine = processedLine.Replace("{AnimalType}", lineFields[(int)Fields.AnimalType]);
                processedLine = processedLine.Replace("{AnimalName}", lineFields[(int)Fields.AnimalName]);
                processedLine = processedLine.Replace("{AnimalChip}", lineFields[(int)Fields.AnimalChip]);
                processedLine = processedLine.Replace("{Breed}", lineFields[(int)Fields.AnimalBreed]);
                processedLine = processedLine.Replace("{OwnerName}", lineFields[(int)Fields.OwnerName]);
                processedLine = processedLine.Replace("{OwnerPhone}", lineFields[(int)Fields.OwnerPhone]);
                processedLine = processedLine.Replace("{VaccinationDate}", lineFields[(int)Fields.VaccinationDate]);
                processedLine = processedLine.Replace("{AggressiveState}", lineFields[(int)Fields.AgressiveState]);

                htmlContent.Append(processedLine);
            }

            htmlContent.Append(footerContent);

            return htmlContent.ToString();
        }

    }
}
